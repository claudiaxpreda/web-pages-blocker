###Claudia Preda
##Readme

# Web Pages Blocker

*	This is a Python script developed for persoanl use as part of my attempts to learn and understand Python,
	It was desigend with the help of some tutorials;
*	It is an application which blocks given website pages, editing /etc/hosts file, in fact blocking IP addresses;
* 	The websites added in websites_to_block.txt file are rerouted to 127.0.0.1.


###Implementation:

*	There are two scripts *hosts.py*, where the main actions are done. Here are the requests for web-pages
	rerouted in a while(TRUE) loop. This script can be manually stopped or using a parameter in the following
	format: hh:mm (basically a moment of time, an hour);
*	Every iteration of the while loop is executed at evry 10 seconfds, this time can be changed;

*	The second script *block_webistes.py* is the one called by the user. First it is necessary
	to use the *pgrep -f hosts.py* command, which returns the PIDs of the processed started by *hosts.py*. 
* 	Based on some options, the program si started, running in background or stopped; (STOP/START);

###Commands: 
*	sudo python block_websites.py start ( For starting the application);
*	sudo python block_websites.py start hh:mm ( For starting the application with a stop point);
*	sudo python block_websites.py stop ( For ending/killing the program.

	
