import sys
import time
from datetime import datetime as dt

hosts_path = "/etc/hosts"
localhost = "127.0.0.1"  # Localhost's IP
end_time = [-1, -1]


def update_hostnames():
    with open(hosts_path, "r+") as file:
        content = file.read()
        websites_list = get_websites_list()
        for website in websites_list:
            if website in content:
                pass
            else:
                # Map hostnames to localhost IP address
                file.write(localhost + " " + website + "\n")


def remove_hostnames():
    with open(hosts_path, "r+") as file:
        content = file.readlines()
        file.seek(0)
        websites_list = get_websites_list()
        for line in content:
            if not any(website in line for website in websites_list):
                file.write(line)

        file.truncate()  # Remove hostnames from host file


def get_websites_list():
    with open("websites_to_block.txt", "r") as file:
        websites_list = [line.strip() for line in file]  # Remove newlines
    return websites_list


def main():
    global end_time
    if len(sys.argv) == 2:
        end_time = sys.argv[1].split(":")
        end_time[0] = int(end_time[0])
        end_time[1] = int(end_time[1])

    while True:
        websites_list = get_websites_list()

        if dt.now().hour != end_time[0] or dt.now().minute != end_time[1]:
            update_hostnames()
        else:
            remove_hostnames()
            sys.exit()
        time.sleep(10)


if __name__ == "__main__":
    main()
