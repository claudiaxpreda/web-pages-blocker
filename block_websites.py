#!/usr/bin/python
from subprocess import check_output

import hosts as hst
import os
import sys

# Gets the output from the pgrep -f command, which look up processes based on name
# Removes trailing newline
# Splits the string into a list of PIDs, where the last element is the PID of this script
ids_list = os.popen("pgrep -f hosts.py").read().strip().split("\n")

if len(sys.argv) != 2 and len(sys.argv) != 3:
    print("Incorrect number of arguments")
else:
    if sys.argv[1] == "start":
        if len(ids_list) == 1:
            time = ""
            if len(sys.argv) == 3:
                time = sys.argv[2]
            # Starts host.py script in background
            os.system("python hosts.py " + time + " &")
            print("hosts.py has started running")
        else:
            print("hosts.py is already running")
    if sys.argv[1] == "stop":
        if len(ids_list) == 1:
            print("hosts.py is not running")
        else:
            for id in ids_list[:-1]:  # Iterate the list, except the last element
                os.system("kill " + id)
            hst.remove_hostnames()
            print("hosts.py has been stopped")
